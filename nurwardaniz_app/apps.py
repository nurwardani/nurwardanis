from django.apps import AppConfig


class NurwardanizProjectConfig(AppConfig):
    name = 'nurwardaniz_app'
