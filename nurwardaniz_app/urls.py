from django.urls import path,include
from nurwardaniz_app import views

urlpatterns = [
    path('', views.home, name='index'),
    path('about/', views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('portofolio/ ', views.portofolio, name='portofolio'),
    path('educations/', views.educations, name='educations'),
    path('experiences/', views.experiences, name='experiences'),  
]