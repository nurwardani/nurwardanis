from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'index.html', {})
def about(request):
    return render(request, 'about.html', {})
def contact(request):
    return render(request, 'contact.html', {})
def portofolio(request):
    return render(request, 'portofolio.html', {})
def educations(request):
    return render(request, 'educations.html', {})
def experiences(request):
    return render(request, 'experiences.html', {})