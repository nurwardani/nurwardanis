from django.db import models
import datetime

# Create your models here.
class jadwal(models.Model):
     kegiatan    = models.CharField(max_length =50)
     tanggal     = models.DateField()
     waktu       = models.TimeField(default=datetime.time(00, 00))
     tempat      = models.CharField(max_length=31)
     
     LIST_CATEGORY = (
        ('Belajar','belajar'),
        ('Makan','makan'),
        ('Refreshing','refreshing')

     )
     
     
     kategori    = models.CharField(
         max_length = 20,
         choices    = LIST_CATEGORY,
     )

     def __str__(self):
         return "{}.{}".format(self.id,self.kegiatan)
     