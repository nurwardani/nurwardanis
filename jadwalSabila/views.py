from django.shortcuts import render,redirect

# Create your views here.
from .forms import jadwalForm
from .models import jadwal


def delete(request,delete_id):
    jadwal.objects.filter(id=delete_id).delete()
    return redirect('daftarJadwal')


def jadwalSabila(request):
    semuaJadwal  = jadwal.objects.all()

    context = {
        'semuaJadwal'   : semuaJadwal,
    }

    return render(request,'jadwalSabila.html',context)

def formJadwal(request):
    form_jadwal = jadwalForm(request.POST or None)

    if request.method =='POST':
        if form_jadwal.is_valid():
            form_jadwal.save()

            return redirect('daftarJadwal')
        
    context = {
        'form_jadwal' : form_jadwal
    }

    return render(request,'formJadwal.html',context)

