from django import forms

from .models import jadwal

class jadwalForm(forms.ModelForm):
    class Meta :
        model   = jadwal
        fields  = [
            'kegiatan',
            'tanggal',
            'waktu',
            'tempat',
            'kategori',
        ]

        widgets = {
            'kegiatan' : forms.TextInput(
                attrs={
                    'class' : 'form-control',

                }
            ),

            'tanggal' : forms.DateInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' :'YYYY-MM-DD'
                }

            ),

            'waktu' : forms.TimeInput(
                attrs={
                    'class' : 'form-control',
                }

            ),

            'tempat' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                }
            ),

            'kategori' : forms.Select(
                attrs={
                    'class' : 'form-control',
                }
            ),
        }