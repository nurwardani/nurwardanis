# Generated by Django 2.0.6 on 2019-10-08 05:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jadwalSabila', '0003_auto_20191008_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwal',
            name='kategori',
            field=models.CharField(choices=[('Belajar', 'belajar'), ('Makan', 'makan'), ('Refreshing', 'refreshing')], max_length=20),
        ),
    ]
