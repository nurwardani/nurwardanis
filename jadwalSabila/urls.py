from django.urls import path,include

from . import views

urlpatterns = [
   path('', views.jadwalSabila, name='daftarJadwal'),
   path('formJadwal/', views.formJadwal, name = 'formJadwal'),
   path('delete/<int:delete_id>', views.delete, name = 'delete'),
]


